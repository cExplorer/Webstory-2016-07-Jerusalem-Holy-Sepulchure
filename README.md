# A Webstory

Thankfully based on the

Hallooou - One page HTML5 responsive template
[Live Demo](http://bit.ly/ht_preview)

Visit the Webstory at
[pilgerweg-21](https://pilgerweg-21.de/webstories/2016-07-Jerusalem-Holy-Sepulchure/)

## Prevent access to git data

Rename the file _htaccess to .htaccess, if a directive for deny access git data is not present in a generell .htaccess, maybe in the root of public html data. Make sure the RewriteEngine of apache is active.
